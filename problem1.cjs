function matchingCarId(inventory, id) {
  if (id == undefined) {
    console.error("id is undefined");
    return [];
  } else if (!Number.isInteger(id)) {
    console.error("id is not a number: "+id);
    return [];
  } else if (inventory == undefined) {
    console.error("inventory is undefined");
    return [];
  } else if (Array.isArray(inventory)) {
    if (Array.length != 0) {
      for (let index = 0; index < inventory.length; index++) {
        if (id == inventory[index].id) {
          return inventory[index];
        }
      }
    } else {
      console.error("empty inventory");
      return [];
    }
  } else {
    console.error("inventory is not an array");
    return [];
  }
}

module.exports = matchingCarId;
